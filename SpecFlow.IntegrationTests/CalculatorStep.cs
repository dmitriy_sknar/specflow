using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SpecFlow.IntegrationTests {
    [Binding]
    public class CalculatorStep {
        private Calculator calculator;
        private int result;
        private ScenarioContext scenarioContext;

        public CalculatorStep(ScenarioContext scenarioContext, Calculator calculator) {
            this.scenarioContext = scenarioContext;
            this.calculator = calculator;
        }

        [BeforeScenario]
        public void InitializeObject() {

        }

        [Given(@"The calculator")]
        public void GivenTheCalculator() {
            calculator = new Calculator();
        }

        [Given(@"First number is set to (.*)")]
        public void GivenFirstNumberIsSetTo(int p0) {
            calculator.FirstNumber = p0;
        }

        [Given(@"Second Number is set to (.*)")]
        public void GivenSecondNumberIsSetTo(int p0) {
            calculator.SecondNumber = p0;
        }

        [When(@"Add action is called")]
        public void WhenAddActionIsCalled() {
            result = calculator.Add();
        }

        [Then(@"Result is (.*)")]
        public void ThenResultIs(int expectedResult) {
            // Assert.AreEqual(result, expectedResult, "Add action failed");
            Assert.AreEqual(scenarioContext.Get<int>("result"), expectedResult, "Add action failed");
        }

        [Given(@"FirstNumber is (.*)")]
        public void GivenFirstNumberIs(int fisrtNumber) {
            calculator.FirstNumber = fisrtNumber;
        }

        [Given(@"SecondNumber is (.*)")]
        public void GivenSecondNumberIs(int secondNumber) {
            calculator.SecondNumber = secondNumber;
        }

        [When(@"Substract action is called")]
        public void WhenSubstractActionIsCalled() {
            result = calculator.Substract();
        }

        [Given(@"Two figures")]
        public void GivenTwoFigures(Table table) {
            var figuresForCalculator = table.CreateInstance<FiguresForCalculator>();
            calculator.FirstNumber = figuresForCalculator.FirstNumber;
            calculator.SecondNumber = figuresForCalculator.SecondNumber;
        }

        [Given(@"The FirstNumber is (.*)")]
        public void GivenTheFirstNumberIs(int fisrtNumber) {
            calculator.FirstNumber = fisrtNumber;
        }

        [Given(@"The SecondNumber is (.*)")]
        public void GivenTheSecondNumberIs(int secondNumber) {
            calculator.SecondNumber = secondNumber;
        }

        [When(@"Multiply action is called")]
        public void WhenMultiplyActionIsCalled() {
            result = calculator.Multiply();
            var contextResult = result;
            scenarioContext.Add("result", contextResult);
        }

    }
}