﻿Feature: Calculator
As an accountant
I want to use Calculator to manipulate with two figures
So that I can add, substract, multiply and divide them.

@tag1
Scenario: Add two numbers
	Given The calculator
	And First number is set to 10
	And Second Number is set to 2
	When Add action is called
	Then Result is 12

@Basic
Scenario: Substract two numbers
	Given The calculator
	And FirstNumber is 10
	And SecondNumber is 2
	When Substract action is called
	Then Result is 8

@DataTable
Scenario: Substract one figure from another
	Given The calculator
	And Two figures
	| Field        | Value |
	| FirstNumber  | 10    |
	| SecondNumber | 2     |
	When Substract action is called
	Then Result is 8

@ScenarioOutline
Scenario Outline: Multiply two figures
	Given The FirstNumber is <firstNumber>
	* The SecondNumber is <secondNumber>
	When Multiply action is called 
	Then Result is <expectedResult>

	Examples: 
	| Description     | firstNumber | secondNumber | expectedResult |
	| ZeroCase        | 0           | 0            | 0              |
	| PositiveFigures | 2           | 2            | 4              |
	| NegativeFigures | -2          | -2           | 4              |