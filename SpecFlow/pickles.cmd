﻿IF EXIST "..\packages\Pickles.CommandLine.2.21.0\tools\" (
..\packages\Pickles.CommandLine.2.21.0\tools\pickles.exe --feature-directory=..\SpecFlow.IntegrationTests --output-directory=..\Features --documentation-format=dhtml
) ELSE (
echo "Pickles was not found. Please open your solution and run 'Install-Package Pickles.CommandLine' in 'Package manager console'"
)
pause