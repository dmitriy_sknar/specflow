﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecFlow {
    class Program {
        static void Main(string[] args) {
            var result = new Calculator() {
                FirstNumber = 10,
                SecondNumber = 2
            }.Add();
            Console.WriteLine($"Result is {result}");
            Console.ReadKey();
        }
    }
}